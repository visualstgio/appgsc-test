﻿using AppWPF_Sistematicodjwongs.Interfaces;
using AppWPF_Sistematicodjwongs.Serialization;
using System;

namespace AppWPF_Sistematicodjwongs.Models
{
    [Serializable]
    public class Book : IOFile<Book>
    {
        public string Name { get; set; }
        public string Author { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Editorial { get; set; }
        public string Category { get; set; }

        public Book ReadObjectFromXml(string filePath)
        {
            return XmlSerialization.ReadFromXmlFile<Book>(filePath);
        }

        public void WriteObjectInXml(string filePath)
        {
            XmlSerialization.WriteToXmlFile(filePath, this);
        }
    }
}
