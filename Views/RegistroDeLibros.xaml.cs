﻿using AppWPF_Sistematicodjwongs.Controllers;
using System.Windows;

namespace AppWPF_Sistematicodjwongs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            new MainWindowController(this);
        }
    }
}
