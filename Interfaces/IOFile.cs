﻿namespace AppWPF_Sistematicodjwongs.Interfaces
{
    interface IOFile<T>
    {
        T ReadObjectFromXml(string filePath);
        void WriteObjectInXml(string filePath);
    }
}
