﻿using AppWPF_Sistematicodjwongs.Models;
using Microsoft.Win32;
using System;
using System.Windows;
using System.Windows.Controls;

namespace AppWPF_Sistematicodjwongs.Controllers
{
    class MainWindowController
    {
        private MainWindow Window;
        private SaveFileDialog Savedialog;
        private OpenFileDialog Opendialog;

        public MainWindowController(MainWindow Window)
        {
            this.Window = Window;
            this.Savedialog = new SaveFileDialog();
            this.Opendialog = new OpenFileDialog();
            InitController();
        }

        private void InitController()
        {
            Window.BtnOpen.Click += new RoutedEventHandler(this.BookEventHandler);
            Window.BtnSave.Click += new RoutedEventHandler(this.BookEventHandler);
            Window.ComboCategory.SelectedIndex = 0;
        } 

        private void BookEventHandler(object sender, RoutedEventArgs r)
        {
            Button btn = (Button)sender;
            switch (btn.Name)
            {
                case "BtnOpen":
                    Open();
                    break;
                case "BtnSave":
                    Save();
                    break;
                default:
                    break;
            }
        }

        private void Open()
        {
            Opendialog.Filter = "Xml File (*.xml)|*.xml";
            if (Opendialog.ShowDialog() == true)
            {
                Book book = new Book().ReadObjectFromXml(Opendialog.FileName);
                Window.TxtName.Text = book.Name;
                Window.TxtAuthor.Text = book.Author;
                Window.TxtEditorial.Text = book.Editorial;
                Window.ReleaseDatePicker.SelectedDate = book.ReleaseDate;
                Window.ComboCategory.SelectedValue = book.Category;
            }
        }

        private void Clear()
        {
            Window.TxtName.Text = "";
            Window.TxtAuthor.Text = "";
            Window.TxtEditorial.Text = "";
            Window.ComboCategory.SelectedIndex = 0;
        }

        private void Save()
        {
            Savedialog.Filter = "Xml File (*.xml)|*.xml";
            if (Savedialog.ShowDialog() == true)
            {
                Book book = new Book();
                book.Name = Window.TxtName.Text;
                book.Author = Window.TxtAuthor.Text;
                book.ReleaseDate = (DateTime)Window.ReleaseDatePicker.SelectedDate;
                book.Editorial = Window.TxtEditorial.Text;
                book.Category = Window.ComboCategory.SelectedValue.ToString();
                book.WriteObjectInXml(Savedialog.FileName);
                Clear();
            }
        }
    }
}
